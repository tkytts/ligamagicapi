﻿using HtmlAgilityPack;
using Ligamagic.Crawlers.Exceptions;
using Ligamagic.Model.Responses;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;

namespace Ligamagic.Crawlers.Utils
{
    public class Parser
    {
        public HtmlDocument HtmlDocument { get; set; }

        public Parser(string html)
        {
            HtmlDocument = new HtmlDocument();

            HtmlDocument.LoadHtml(html);
        }

        /// <summary>
        /// Returns true if the page has the stores list.
        /// </summary>
        /// <returns></returns>
        internal bool IsResultPage()
        {
            var node = HtmlDocument.DocumentNode.SelectSingleNode("//article[@id='mob-store']");

            return node != null;
        }

        /// <summary>
        /// Filters the card list for the one we want and returns its URL.
        /// </summary>
        /// <param name="cardName"></param>
        /// <returns></returns>
        internal string GetUrlFromList(string cardName)
        {
            var nodesResult = HtmlDocument.DocumentNode.SelectNodes("//div[@class='panel panel-card' and contains(@onclick, 'card')]");

            if (nodesResult == null)
            {
                throw new CardNotFoundException(cardName);
            }

            foreach (var node in nodesResult)
            {
                var nodeName = node.SelectSingleNode("./div/div[@class=\"card-title remove-padding-r\"]");
                var nodeAuxName = node.SelectSingleNode("./div/div[@class=\"card-title-aux\"]");

                var name = nodeName.InnerText.ToLower();
                var auxName = nodeAuxName?.InnerText.ToLower();

                if (name.Equals(cardName.ToLower()) || auxName.Equals(cardName.ToLower()))
                {

                    var onClick = node.GetAttributeValue("onclick", null);

                    var url = ParseUrlFromOnclick(onClick, cardName);

                    return url;
                }
            }

            throw new CardNotFoundException(cardName);
        }

        private string ParseUrlFromOnclick(string onClick, string cardName)
        {
            var baseUrl = ConfigurationManager.AppSettings["LigamagicBaseSearchByIdUrl"];
            var idMatch = Regex.Match(onClick, @"appScreen.cardOpen\((?<id>\d+)\)");

            if (!idMatch.Success)
            {
                throw new Exception($"Could not find a card named: {cardName}");
            }

            return $"{baseUrl}{idMatch.Groups["id"].Value}";
        }

        /// <summary>
        /// Parses the whole card's page.
        /// </summary>
        /// <param name="responses"></param>
        internal void ParseCardPage(List<LigamagicResponse> responses, string cardName)
        {
            var nodesTr = HtmlDocument.DocumentNode.SelectNodes("//article[@id='mob-store']/div[@class = 'panel-body']/div[@class = 'clearfix']");

            foreach (var nodeTr in nodesTr)
            {
                var card = new CardResponse();
                card.Name = cardName;

                var store = ParseStoreInformation(nodeTr);

                var storeExists = StoreExists(responses, store, out LigamagicResponse response);

                if (!storeExists)
                {
                    response = new LigamagicResponse { Store = ParseStoreInformation(nodeTr), Cards = new List<CardResponse>() };
                }

                ParseCard(nodeTr, card);

                response.Cards.Add(card);

                if (!storeExists)
                {
                    responses.Add(response);
                }
            }
        }

        /// <summary>
        /// Checks if the stores exists in the responses.
        /// </summary>
        /// <param name="responses"></param>
        /// <param name="store"></param>
        /// <returns></returns>
        private bool StoreExists(List<LigamagicResponse> responses, StoreResponse store, out LigamagicResponse response)
        {
            lock (new { })
            {
                response = responses.Where(x => x.Store.Name.ToLower().Equals(store.Name.ToLower())).FirstOrDefault();
            }

            return response != null;
        }

        /// <summary>
        /// Parses the information of a TR and returns a new store object.
        /// </summary>
        /// <param name="nodeTr"></param>
        /// <returns></returns>
        private StoreResponse ParseStoreInformation(HtmlNode nodeTr)
        {
            var store = new StoreResponse();

            var storeNameNode = nodeTr.SelectSingleNode("./div[@class = 'col-md-2 col-sm-2 col-xs-4 store-picture']");
            var storeUrlNode = nodeTr.SelectSingleNode("./div[@class = 'col-md-5 col-sm-3 col-xs-4 form-add-cart']/button");
            var storeIconNode = storeNameNode.SelectSingleNode("./img");

            var storeNameMatch = Regex.Match(storeNameNode.GetAttributeValue("onclick", null), "");

            var baseUrl = ConfigurationManager.AppSettings["LigamagicBaseUrl"];
            var storeId = Regex.Replace(storeUrlNode.GetAttributeValue("id", null), "buy_", "e");

            store.Name = GetCardNameFromNode(storeNameNode);
            store.Url = $"{baseUrl}b/?p={storeId}";
            store.IconUrl = storeIconNode.GetAttributeValue("src", null);

            return store;
        }

        /// <summary>
        /// Returns the card name from a HTML Node.
        /// </summary>
        /// <param name="storeNameNode"></param>
        /// <returns></returns>
        private string GetCardNameFromNode(HtmlNode storeNameNode)
        {
            var onClick = storeNameNode.GetAttributeValue("onclick", null);

            var nameMatch = Regex.Match(onClick, "advsearch\\.storeSearch\\(\"(?<storeName>.+?)[\"\\\\]");

            if (!nameMatch.Success)
            {
                throw new Exception("Card not found.");
            }

            return nameMatch.Groups["storeName"].Value;
        }

        /// <summary>
        /// Parses the card that is contained within given TR node.
        /// </summary>
        /// <param name="nodeTr"></param>
        /// <param name="card"></param>
        private void ParseCard(HtmlNode nodeTr, CardResponse card)
        {
            var cardQuantityNode = nodeTr.SelectSingleNode("./div[contains(text(), 'unid')]");
            var cardSetNode = nodeTr.SelectSingleNode("./div[@class='col-md-2 col-sm-3 col-xs-8 card-details']/div[@class='clearfix']/div[@class='store-card-detais card-edition']");

            var cardPriceNode = nodeTr.SelectSingleNode("./div[@class='col-md-2 col-sm-3 col-xs-8 card-details']/h3");

            var priceString = cardPriceNode.InnerText;

            if (priceString.Contains("OFF"))
            {
                priceString = Regex.Replace(priceString, ".+OFF", string.Empty);
            }

            priceString = Regex.Replace(priceString, @"[^\d,]", string.Empty);
            priceString = Regex.Replace(priceString, ",", ".").Trim('.');

            var quantityString = cardQuantityNode.InnerText;
            quantityString = Regex.Replace(quantityString, @"\D", string.Empty);

            card.Set = cardSetNode.InnerText.Trim();
            card.Copies = int.Parse(quantityString);
            card.Price = float.Parse(priceString);
        }

        internal bool NoStoresSelling()
        {
            var noStoresSellingNode = HtmlDocument.DocumentNode.SelectSingleNode("//div[@class='card-warning-semestoque']");

            return noStoresSellingNode != null;
        }
    }
}