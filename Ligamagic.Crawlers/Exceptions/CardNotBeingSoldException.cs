﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ligamagic.Crawlers.Exceptions
{
    public class CardNotBeingSoldException : Exception
    {
        public CardNotBeingSoldException(string cardName) : base($"There are no stores selling {cardName}.")
        {
        }
    }
}