﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ligamagic.Crawlers.Exceptions
{
    public class CardNotFoundException : Exception
    {
        public CardNotFoundException(string cardName) : base($"{cardName} was not found.")
        {
        }
    }
}