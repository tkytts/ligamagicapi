using Ligamagic.Crawlers.Exceptions;
using Ligamagic.Crawlers.Utils;
using Ligamagic.Model.Requests;
using Ligamagic.Model.Responses;
using Ligamagic.Repository.Contexts;
using Ligamagic.Repository.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Ligamagic.Crawlers
{
	public class LigamagicCrawler
	{
		private static readonly string _baseUrl = ConfigurationManager.AppSettings["LigamagicBaseSearchByNameUrl"];
		private LigamagicContext _db { get; set; }
		private HttpClient _httpClient { get; set; }

		public LigamagicCrawler(LigamagicContext db)
		{
			_db = db ?? throw new ArgumentNullException(nameof(db));
			_httpClient = new HttpClient();
		}

		/// <summary>
		/// Gets the information on all the requested cards.
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		public List<LigamagicResponse> SearchForCards(LigamagicRequest request)
		{
			var responses = new List<LigamagicResponse>();

			var cardsInDb = request.Cards.Where(x => CheckIfUpdated(x)).ToList();

			var cardsNotInDb = from c in request.Cards
							   where !cardsInDb.Any(x => x.Name.Equals(c.Name))
							   select c;

			GetCardInfoFromDb(cardsInDb, ref responses);

			Parallel.ForEach(cardsNotInDb, card =>
			{
				GetCardInfoFromWebsite(card, ref responses);

			});

			new Task(() => AddResponsesToDb(responses.Where(x => !x.IsFromDb))).Start();

			return responses;
		}

		/// <summary>
		/// Checks if the card was updated in the last 24 hours.
		/// </summary>
		/// <param name="card"></param>
		/// <returns></returns>
		private bool CheckIfUpdated(CardRequest card)
		{
			var updateThreshold = DateTime.Now.AddDays(-1);

			var hasCard = _db.PriceTables.Any(x => x.Card.Name.Equals(card.Name) && x.LastUpdate > updateThreshold);

			return hasCard;
		}

		/// <summary>
		/// Gets all the info of an updated card from the database.
		/// </summary>
		/// <param name="card"></param>
		/// <param name="responses"></param>
		private void GetCardInfoFromDb(IEnumerable<CardRequest> cards, ref List<LigamagicResponse> responses)
		{
			foreach (var card in cards)
			{
				var updateThreshold = DateTime.Now.AddDays(-1);
				var priceTables = _db.PriceTables.Where(x => x.Card.Name.ToLower().Equals(card.Name.ToLower()) && x.LastUpdate > updateThreshold);

				foreach (var priceTable in priceTables.ToList())
				{
					LigamagicResponse response;

					var storeExists = StoreExists(priceTable.Store, responses, out response);

					if (!storeExists)
					{
						AddStoreToResponse(priceTable, responses);
					}
					else
					{
						AddCardToStore(priceTable, response);
					}
				}
			}
		}

		/// <summary>
		/// Check if the store exists on the db.
		/// </summary>
		/// <param name="store"></param>
		/// <param name="responses"></param>
		/// <param name="response"></param>
		/// <returns></returns>
		private bool StoreExists(LMG_Store store, List<LigamagicResponse> responses, out LigamagicResponse response)
		{
			response = responses.FirstOrDefault(x => x.Store.Name.ToLower().Equals(store.Name.ToLower()));

			return response != null;
		}

		/// <summary>
		/// Creates and adds a new response to the list.
		/// </summary>
		/// <param name="priceTable"></param>
		/// <param name="responses"></param>
		private void AddStoreToResponse(GLB_PriceTable priceTable, List<LigamagicResponse> responses)
		{
			var store = new StoreResponse
			{
				Name = priceTable.Store.Name,
				Url = priceTable.Store.Url,
				IconUrl = priceTable.Store.IconUrl
			};

			var cards = new List<CardResponse>();

			cards.Add(new CardResponse
			{
				Name = priceTable.Card.Name,
				Set = priceTable.Card.Set,
				Copies = priceTable.Copies,
				Price = priceTable.Price
			});

			responses.Add(new LigamagicResponse
			{
				Store = store,
				Cards = cards,
				IsFromDb = true
			});
		}

		/// <summary>
		/// Adds a single card to a response.
		/// </summary>
		/// <param name="priceTable"></param>
		/// <param name="response"></param>
		private void AddCardToStore(GLB_PriceTable priceTable, LigamagicResponse response)
		{
			var card = new CardResponse
			{
				Name = priceTable.Card.Name,
				Set = priceTable.Card.Set,
				Copies = priceTable.Copies,
				Price = priceTable.Price
			};

			response.Cards.Add(card);
		}

		/// <summary>
		/// Gets all the info from the website.
		/// </summary>
		/// <param name="card"></param>
		/// <param name="responses"></param>
		private void GetCardInfoFromWebsite(CardRequest card, ref List<LigamagicResponse> responses)
		{
			var url = GetCardUrl(card.Name);
			var html = _httpClient.Get(url);
			var parser = new Parser(html);

			if (string.IsNullOrEmpty(html))
			{
				throw new CardNotBeingSoldException(card.Name);
			}

			if (!parser.IsResultPage())
			{
				html = GetCardFromList(parser, card.Name);

				parser = new Parser(html);

				if (!parser.IsResultPage())
				{
					throw new Exception($"A carta {card.Name} não foi encontrada.");
				}
			}

			parser.ParseCardPage(responses, card.Name);
		}

		/// <summary>
		/// Gets the URL used for searching.
		/// </summary>
		/// <param name="cardName"></param>
		/// <returns></returns>
		private string GetCardUrl(string cardName)
		{
			return $"{_baseUrl}{GetSearchTerm(cardName)}";
		}

		/// <summary>
		/// Returns the formatted name used for the URL.
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		private object GetSearchTerm(string name)
		{
			return Regex.Replace(name.ToLower(), @"\s", "+");
		}

		/// <summary>
		/// Gets the searched card's page from a list of cards.
		/// </summary>
		/// <param name="parser"></param>
		/// <param name="cardName"></param>
		/// <returns></returns>
		private string GetCardFromList(Parser parser, string cardName)
		{
			var url = parser.GetUrlFromList(cardName);

			return _httpClient.Get(url);
		}

		/// <summary>
		/// Adds all found information to the database.
		/// </summary>
		/// <param name="responses"></param>
		private void AddResponsesToDb(IEnumerable<LigamagicResponse> responses)
		{
			var cardsInDb = new List<MTG_Card>();

			foreach (var response in responses)
			{
				var storeDb = _db.Stores.Where(x => x.Name.Equals(response.Store.Name)).FirstOrDefault();

				if (storeDb == null)
				{
					storeDb = new LMG_Store
					{
						Name = response.Store.Name,
						Url = response.Store.Url,
						IconUrl = response.Store.IconUrl
					};
				}

				foreach (var card in response.Cards)
				{
					var guid = GetCardGuid(card.Name, card.Set);

					var cardDb = _db.Cards.Where(x => x.Guid.Equals(guid)).FirstOrDefault();

					if (cardDb == null)
					{
						cardDb = new MTG_Card
						{
							Guid = guid,
							Name = card.Name,
							Set = card.Set
						};
					}

					var priceTable = new GLB_PriceTable
					{
						Card = cardDb,
						Store = storeDb,
						Copies = card.Copies,
						Price = card.Price,
						LastUpdate = DateTime.Now
					};

					if (_db.PriceTables.Any(x => x.Card.Name.ToLower().Equals(cardDb.Name.ToLower()) && x.Store.Name.ToLower().Equals(storeDb.Name.ToLower())))
					{
						UpdatePriceTable(priceTable);
					}
					else
					{
						_db.PriceTables.Add(priceTable);
					}

					_db.SaveChanges();
				}
			}

		}

		/// <summary>
		/// Returns a Guid for a card.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="set"></param>
		/// <returns></returns>
		private string GetCardGuid(string name, string set)
		{
			var md5 = MD5.Create();
			var bytes = Encoding.UTF8.GetBytes($"{name.ToLower()}{set.ToLower()}");

			return new Guid(md5.ComputeHash(bytes)).ToString();
		}

		/// <summary>
		/// Updates a PriceTable in the database.
		/// </summary>
		/// <param name="priceTable"></param>
		private void UpdatePriceTable(GLB_PriceTable priceTable)
		{
			var priceTableDb = _db.PriceTables.Where(x => x.Card.Name.ToLower().Equals(priceTable.Card.Name.ToLower()) && x.Store.Name.ToLower().Equals(priceTable.Store.Name.ToLower())).FirstOrDefault();

			priceTableDb.Copies = priceTable.Copies;
			priceTableDb.Price = priceTable.Price;
			priceTableDb.LastUpdate = DateTime.Now;
		}
	}
}