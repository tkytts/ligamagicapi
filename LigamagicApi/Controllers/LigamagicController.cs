﻿using Ligamagic.Crawlers;
using Ligamagic.Model.Requests;
using Ligamagic.Repository.Contexts;
using System;
using System.Web.Http;

namespace LitamagicApi.Controllers
{
	public class LigamagicController : ApiController
	{
		[HttpPost]
		public IHttpActionResult SearchCards(LigamagicRequest request)
		{
			try
			{
				var crawler = new LigamagicCrawler(new LigamagicContext());

				return Ok(crawler.SearchForCards(request));
			}
			catch (Exception e)
			{
				return Ok(e.Message);
			}
		}
	}
}
