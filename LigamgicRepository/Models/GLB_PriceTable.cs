﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ligamagic.Repository.Models
{
    public class GLB_PriceTable
    {
        public int Id { get; set; }
        public virtual MTG_Card Card { get; set; }
        public virtual LMG_Store Store { get; set; }
        public float Price { get; set; }
        public int Copies { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}