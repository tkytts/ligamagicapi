﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ligamagic.Repository.Models
{
    public class MTG_Card
    {
        [Key]
        public string Guid { get; set; }
        public string Name { get; set; }
        public string Set { get; set; }
    }
}