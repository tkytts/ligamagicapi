﻿using Ligamagic.Repository.Models;
using System.Data.Entity;

namespace Ligamagic.Repository.Contexts
{
	public class LigamagicContext : DbContext
	{
		public LigamagicContext() : base("MTGDbo")
		{
		}

		public DbSet<MTG_Card> Cards { get; set; }
		public DbSet<LMG_Store> Stores { get; set; }
		public DbSet<GLB_PriceTable> PriceTables { get; set; }
	}
}