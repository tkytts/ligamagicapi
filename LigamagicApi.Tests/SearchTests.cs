﻿using Ligamagic.Crawlers;
using Ligamagic.Crawlers.Exceptions;
using Ligamagic.Model.Requests;
using Ligamagic.Repository.Contexts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace LigamagicApi.Tests
{
	[TestClass]
	public class SearchTests
	{
		[TestMethod]
		public void SearchTest()
		{
			var crawler = new LigamagicCrawler(new LigamagicContext());

			var cards = new List<CardRequest>
			{
				new CardRequest
				{
					Name = "Opt",
					Set = "Iconic Masters",
					Copies = 1
				}
			};

			crawler.SearchForCards(new LigamagicRequest
			{
				Cards = cards,
				SatisfactoryPercentage = 0.7f
			});
		}

		[TestMethod]
		[ExpectedException(typeof(CardNotFoundException))]
		public void CardNotFoundTest()
		{
			var crawler = new LigamagicCrawler(new LigamagicContext());

			var cards = new List<CardRequest>
			{
				new CardRequest
				{
					Name = "ThisCardWillNotBeFound",
					Set = "Dominaria",
					Copies = 1
				}
			};

			try
			{
				crawler.SearchForCards(new LigamagicRequest
				{
					Cards = cards,
					SatisfactoryPercentage = 0.7f
				});
			}
			catch (System.Exception e)
			{
				var innerException = e.InnerException;

				throw innerException;
			}
		}

		[TestMethod]
		[ExpectedException(typeof(CardNotBeingSoldException))]
		public void CardNotSoldTest()
		{
			var crawler = new LigamagicCrawler(new LigamagicContext());

			var cards = new List<CardRequest>
			{
				new CardRequest
				{
					Name = "Black Lotus",
					Set = "Dominaria",
					Copies = 1
				}
			};

			try
			{
				crawler.SearchForCards(new LigamagicRequest
				{
					Cards = cards,
					SatisfactoryPercentage = 0.7f
				});
			}
			catch (System.Exception e)
			{
				var innerException = e.InnerException;

				throw innerException;
			}
		}
	}
}
