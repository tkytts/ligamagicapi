﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ligamagic.Model.Requests
{
    public class LigamagicRequest
    {
        public List<CardRequest> Cards { get; set; }
        public float SatisfactoryPercentage { get; set; }
    }
}