﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ligamagic.Model.Requests
{
    public class CardRequest
    {
        public string Name { get; set; }
        public string Set { get; set; }
        public int Copies { get; set; }
    }
}