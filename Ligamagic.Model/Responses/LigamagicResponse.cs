﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ligamagic.Model.Responses
{
    public class LigamagicResponse
    {
        public LigamagicResponse()
        {
            this.IsFromDb = false;
        }

        public StoreResponse Store { get; set; }
        public List<CardResponse> Cards { get; set; }
        public bool IsFromDb { get; set; }
    }
}