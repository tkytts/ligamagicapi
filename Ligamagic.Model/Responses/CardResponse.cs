﻿namespace Ligamagic.Model.Responses
{
    public class CardResponse
    {
        public string Name { get; set; }
        public string Set { get; set; }
        public float Price { get; set; }
        public int Copies { get; set; }
    }
}