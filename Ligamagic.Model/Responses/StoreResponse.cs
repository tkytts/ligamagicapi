﻿namespace Ligamagic.Model.Responses
{
    public class StoreResponse
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string IconUrl { get; set; }
    }
}